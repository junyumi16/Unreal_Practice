// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "GrabberSome.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_GrabberSome_generated_h
#error "GrabberSome.generated.h already included, missing '#pragma once' in GrabberSome.h"
#endif
#define MYPROJECT_GrabberSome_generated_h

#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_SPARSE_DATA
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRelease); \
	DECLARE_FUNCTION(execGrab);


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGrabberSome(); \
	friend struct Z_Construct_UClass_UGrabberSome_Statics; \
public: \
	DECLARE_CLASS(UGrabberSome, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UGrabberSome)


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGrabberSome(UGrabberSome&&); \
	NO_API UGrabberSome(const UGrabberSome&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGrabberSome); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGrabberSome); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGrabberSome) \
	NO_API virtual ~UGrabberSome();


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_11_PROLOG
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_SPARSE_DATA \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_INCLASS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class UGrabberSome>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_GrabberSome_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
