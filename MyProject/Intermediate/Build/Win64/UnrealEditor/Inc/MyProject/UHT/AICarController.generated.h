// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "AICarController.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_AICarController_generated_h
#error "AICarController.generated.h already included, missing '#pragma once' in AICarController.h"
#endif
#define MYPROJECT_AICarController_generated_h

#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_SPARSE_DATA
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAICarController(); \
	friend struct Z_Construct_UClass_UAICarController_Statics; \
public: \
	DECLARE_CLASS(UAICarController, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UAICarController)


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAICarController(UAICarController&&); \
	NO_API UAICarController(const UAICarController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAICarController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAICarController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAICarController) \
	NO_API virtual ~UAICarController();


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_10_PROLOG
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_SPARSE_DATA \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_INCLASS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class UAICarController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_AICarController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
