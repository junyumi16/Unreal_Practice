// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Finder.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_Finder_generated_h
#error "Finder.generated.h already included, missing '#pragma once' in Finder.h"
#endif
#define MYPROJECT_Finder_generated_h

#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_SPARSE_DATA
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFind_Coffee); \
	DECLARE_FUNCTION(execFind_Car); \
	DECLARE_FUNCTION(execFind_People); \
	DECLARE_FUNCTION(execFind);


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFinder(); \
	friend struct Z_Construct_UClass_UFinder_Statics; \
public: \
	DECLARE_CLASS(UFinder, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UFinder)


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFinder(UFinder&&); \
	NO_API UFinder(const UFinder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFinder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFinder); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UFinder) \
	NO_API virtual ~UFinder();


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_10_PROLOG
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_SPARSE_DATA \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_INCLASS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class UFinder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_Finder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
