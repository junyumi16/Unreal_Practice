// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "YourMovingActor.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_YourMovingActor_generated_h
#error "YourMovingActor.generated.h already included, missing '#pragma once' in YourMovingActor.h"
#endif
#define MYPROJECT_YourMovingActor_generated_h

#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_SPARSE_DATA
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_ACCESSORS
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUYourMovingActor(); \
	friend struct Z_Construct_UClass_UYourMovingActor_Statics; \
public: \
	DECLARE_CLASS(UYourMovingActor, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(UYourMovingActor)


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UYourMovingActor(UYourMovingActor&&); \
	NO_API UYourMovingActor(const UYourMovingActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UYourMovingActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UYourMovingActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UYourMovingActor) \
	NO_API virtual ~UYourMovingActor();


#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_10_PROLOG
#define FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_SPARSE_DATA \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_ACCESSORS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_INCLASS_NO_PURE_DECLS \
	FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class UYourMovingActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_yumi_UnrealPractice_Unreal_Practice_MyProject_Source_MyProject_YourMovingActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
